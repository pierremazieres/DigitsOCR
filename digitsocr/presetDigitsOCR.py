#!/usr/bin/env python3
# coding=utf-8
# import
from neuralnetworkcommon.cli.cli import loadArchive
from gzip import decompress
from json import loads
from random import randrange
from tkinter import Button, Variable, Listbox
from functools import partial
from digitsocr.commonDigitsOCR import PICTURE_LENGTH, SCALING_FACTOR, DIGITS_RANGE, refreshResponse, callNeuralNetworkService, displayResponse, mainWindow, commandFrame, canvas, RECOGNIZE, DIGITS_SET_FILE
# constants
DIGITS_LIST = None
# load digits list
def loadDigitsList():
    print("loading digits file")
    compressedDigitsSet = loadArchive(DIGITS_SET_FILE)
    global DIGITS_LIST
    DIGITS_LIST = loads(decompress(compressedDigitsSet).decode())
    pass
# handle digit
def getRandomPicture(digit):
    pictures = DIGITS_LIST[digit]
    randomIndex = randrange(len(pictures))
    return pictures[randomIndex]
    pass
def drawDigit(picture):
    x = 0
    y = 0
    for grayLevel in picture:
        # draw pixel
        startX = x * SCALING_FACTOR
        startY = y * SCALING_FACTOR
        endX = (x + 1) * SCALING_FACTOR
        endY = (y + 1) * SCALING_FACTOR
        color = "gray"+str(int((1-grayLevel)*100))
        canvas.create_rectangle((startX, startY), (endX, endY), outline=color, fill=color)
        # next coordonante
        x += 1
        if x == PICTURE_LENGTH:
            x = 0
            y += 1
        pass
    pass
# main window construction
def recognize():
    print("recognize")
    selectedDigits = digitsListBox.curselection()
    # a digit must be selected
    if len(selectedDigits) > 0:
        selectedDigit = selectedDigits[0]
        print("selected digit : {}".format(selectedDigit))
        picture = getRandomPicture(selectedDigit)
        drawDigit(picture)
        outputVector = callNeuralNetworkService(picture)
        refreshResponse()
        displayResponse(outputVector)
        pass
    pass
availableDigits = Variable(commandFrame, list(DIGITS_RANGE))
digitsListBox = Listbox(commandFrame, listvariable=availableDigits)
digitsListBox.grid(row=0, column=0)
recognizeButton = Button(commandFrame, text=RECOGNIZE, command=partial(recognize))
recognizeButton.grid(row=1, column=0)
# run program
digitsList = loadDigitsList()
mainWindow.mainloop()