#!/usr/bin/env python3
# coding=utf-8
# import
from tkinter import Radiobutton, Label, StringVar, Button
from functools import partial
from enum import Enum, unique
from copy import deepcopy
from digitsocr.commonDigitsOCR import PICTURE_LENGTH, SCALING_FACTOR, CANVAS_SIZE, refreshResponse, callNeuralNetworkService, displayResponse, mainWindow, commandFrame, canvas, RECOGNIZE
# constants
IMAGE_RANGE = range(PICTURE_LENGTH)
@unique
class DrawAction(Enum):
    BOLD_LINE="bold line"
    MEDIUM_LINE="medium line"
    THIN_LINE="thin line"
    BOLD_ERASER="bold eraser"
    MEDIUM_ERASER="medium eraser"
    THIN_ERASER="thin eraser"
@unique
class GrayLevel(Enum):
    BLACK=1
    DARK=3/4
    MEDIUM=1/2
    LIGHT=1/4
    WHITE=0
# shared variables
selectedAction = None
lastAction = None
lastPixels = set()
actualBitmap = list()
lastBitmap = list()
# drawing methods
def resetBitmaps():
    global actualBitmap
    global lastBitmap
    actualBitmap = [[GrayLevel.WHITE.value] * PICTURE_LENGTH for _ in IMAGE_RANGE]
    lastBitmap = deepcopy(actualBitmap)
    pass
def getPosition(canvasX,canvasY):
    x=int(canvasX/SCALING_FACTOR)
    y=int(canvasY/SCALING_FACTOR)
    print("logical pixel : x={} ; y={}".format(x, y))
    return x, y
def isPositionValid(x,y):
    return x>0 and x<PICTURE_LENGTH and y>0 and y<PICTURE_LENGTH
def setPositionsGrayLevel(positions,grayLevel):
    for position in positions:
        x = position[0]
        y = position[1]
        grayLevelValue = grayLevel.value
        if isPositionValid(x,y) and (grayLevelValue == GrayLevel.WHITE.value or grayLevelValue>actualBitmap[x][y]):
            actualBitmap[x][y] = grayLevelValue
            pass
        pass
    pass
def drawBold(x,y):
    blackPositions={(x,y)}
    setPositionsGrayLevel(blackPositions, GrayLevel.BLACK)
    deepPositions={(x+1,y),(x-1,y),(x,y+1),(x,y-1)}
    setPositionsGrayLevel(deepPositions, GrayLevel.DARK)
    lightPositions={(x+1,y+1),(x+1,y-1),(x-1,y+1),(x-1,y-1),(x+2,y),(x-2,y),(x,y+2),(x,y-2)}
    setPositionsGrayLevel(lightPositions, GrayLevel.LIGHT)
    pass
def drawMedium(x,y):
    blackPositions={(x,y)}
    setPositionsGrayLevel(blackPositions, GrayLevel.BLACK)
    mediumPositions={(x+1,y),(x-1,y),(x,y+1),(x,y-1)}
    setPositionsGrayLevel(mediumPositions, GrayLevel.MEDIUM)
    pass
def drawThin(x,y):
    blackPositions={(x,y)}
    setPositionsGrayLevel(blackPositions, GrayLevel.BLACK)
    pass
def eraseMany(x,y,dr):
    whitePositions=set()
    for dx in dr:
        for dy in dr:
            whitePositions.add((x+dx,y+dy))
            pass
        pass
    setPositionsGrayLevel(whitePositions, GrayLevel.WHITE)
    pass
def eraseBold(x,y):
    eraseMany(x, y, range(-2,3))
    pass
def eraseMedium(x,y):
    eraseMany(x, y, range(-1,2))
    pass
def eraseThin(x,y):
    whitePositions={(x,y)}
    setPositionsGrayLevel(whitePositions, GrayLevel.WHITE)
    pass
def printBitmap(bitmap):
    stringBitmap=''
    for y in IMAGE_RANGE:
        for x in IMAGE_RANGE:
            grayLevel=bitmap[x][y]
            # INFO : non ascii characters can not be display on some terminals
            stringPixel = '.'#'∙'
            if grayLevel==GrayLevel.BLACK.value:
                stringPixel = '3'#'█'
            elif grayLevel==GrayLevel.DARK.value:
                stringPixel = '2'#'▓'
            elif grayLevel==GrayLevel.MEDIUM.value:
                stringPixel = '1'#'▒'
            elif grayLevel==GrayLevel.LIGHT.value:
                stringPixel = '0'#'░'
            elif grayLevel==GrayLevel.WHITE.value:
                stringPixel = ' '
            stringBitmap+=stringPixel
            pass
        stringBitmap+='\n'
        pass
    print(stringBitmap)
    pass
def dispatchDrawAction(x,y):
    action=selectedAction.get()
    print("action : {}".format(action))
    if action==DrawAction.BOLD_LINE.value:
        drawBold(x,y)
        pass
    elif action==DrawAction.MEDIUM_LINE.value:
        drawMedium(x,y)
        pass
    elif action == DrawAction.THIN_LINE.value:
        drawThin(x,y)
        pass
    elif action == DrawAction.BOLD_ERASER.value:
        eraseBold(x,y)
        pass
    elif action == DrawAction.MEDIUM_ERASER.value:
        eraseMedium(x,y)
        pass
    elif action == DrawAction.THIN_ERASER.value:
        eraseThin(x,y)
        pass
    else :
        pass
    print("actual bitmap")
    printBitmap(actualBitmap)
    pass
def getDifferentialBitmap():
    differentialBitmap = [[None] * PICTURE_LENGTH for i in IMAGE_RANGE]
    for x in IMAGE_RANGE:
        for y in IMAGE_RANGE:
            actualGrayLevel = actualBitmap[x][y]
            lastGrayLevel = lastBitmap[x][y]
            if actualGrayLevel != lastGrayLevel:
                differentialBitmap[x][y] = actualGrayLevel
                pass
            pass
        pass
    print("differential bitmap")
    printBitmap(differentialBitmap)
    return differentialBitmap
def shrinkDifferentialBitmap(rawDifferentialBitmap):
    shrinkedDifferentialBitmap = {
        GrayLevel.BLACK:set(),
        GrayLevel.DARK: set(),
        GrayLevel.MEDIUM: set(),
        GrayLevel.LIGHT: set(),
        GrayLevel.WHITE: set(),
    }
    for x in IMAGE_RANGE:
        for y in IMAGE_RANGE:
            pixelValue=rawDifferentialBitmap[x][y]
            if pixelValue is not None:
                key = None
                if pixelValue==GrayLevel.BLACK.value:
                    key = GrayLevel.BLACK
                elif pixelValue==GrayLevel.DARK.value:
                    key = GrayLevel.DARK
                elif pixelValue==GrayLevel.MEDIUM.value:
                    key = GrayLevel.MEDIUM
                elif pixelValue==GrayLevel.LIGHT.value:
                    key = GrayLevel.LIGHT
                elif pixelValue==GrayLevel.WHITE.value:
                    key = GrayLevel.WHITE
                if key and key in shrinkedDifferentialBitmap:
                    shrinkedDifferentialBitmap[key].add((x, y))
                pass
            pass
        pass
    return shrinkedDifferentialBitmap
def updateCanvas(shrinkedDifferentialBitmap):
    for grayLevel, positions in shrinkedDifferentialBitmap.items():
        color = "white"
        if grayLevel == GrayLevel.BLACK:
            color = "black"
        elif grayLevel == GrayLevel.DARK:
            color = "gray25"
        elif grayLevel == GrayLevel.MEDIUM:
            color = "gray50"
        elif grayLevel == GrayLevel.LIGHT:
            color = "gray75"
        for position in positions:
            x = position[0]
            y = position[1]
            startX = x*SCALING_FACTOR
            startY = y*SCALING_FACTOR
            endX = (x+1)*SCALING_FACTOR
            endY = (y+1)*SCALING_FACTOR
            canvas.create_rectangle((startX, startY), (endX, endY), outline=color, fill=color)
        pass
    pass
def setRawInputVector():
    rawInputVector = list()
    for x in IMAGE_RANGE:
        for y in IMAGE_RANGE:
            rawInputVector.append(actualBitmap[x][y])
            pass
        pass
    return rawInputVector
# main window construction
def draw(event):
    # get logical pixel
    print("physical pixel : x={} ; y={}".format(event.x, event.y))
    x, y = getPosition(event.x, event.y)
    # check for change
    global lastAction
    global lastPixels
    global lastBitmap
    currentAction=selectedAction.get()
    if lastPixels != (x, y) or lastAction != currentAction:
        # apply changes
        dispatchDrawAction(x, y)
        rawDifferentialBitmap = getDifferentialBitmap()
        shrinkedDifferentialBitmap = shrinkDifferentialBitmap(rawDifferentialBitmap)
        updateCanvas(shrinkedDifferentialBitmap)
        # memorize current state
        lastPixels = (x, y)
        lastAction = currentAction
        lastBitmap = deepcopy(actualBitmap)
        pass
    else:
        print("no change")
    pass
def recognize():
    print("recognize")
    refreshResponse()
    rawInputVector = setRawInputVector()
    outputVector = callNeuralNetworkService(rawInputVector)
    refreshResponse()
    displayResponse(outputVector)
    pass
def clean():
    print("clean")
    resetBitmaps()
    canvas.create_rectangle((0, 0), (CANVAS_SIZE, CANVAS_SIZE), outline="white", fill="white")
    refreshResponse()
    pass
canvas.bind('<B1-Motion>', draw)
selectedAction = StringVar(mainWindow,DrawAction.BOLD_LINE.value)
boldLineRadioButton = Radiobutton(commandFrame,variable=selectedAction,value=DrawAction.BOLD_LINE.value)
boldLineRadioButton.grid(row=0, column=0)
boldLineLabel = Label(commandFrame, text=DrawAction.BOLD_LINE.value)
boldLineLabel.grid(row=0, column=1)
boldLineRadioButton = Radiobutton(commandFrame,variable=selectedAction,value=DrawAction.MEDIUM_LINE.value)
boldLineRadioButton.grid(row=1, column=0)
boldLineLabel = Label(commandFrame, text=DrawAction.MEDIUM_LINE.value)
boldLineLabel.grid(row=1, column=1)
boldLineRadioButton = Radiobutton(commandFrame,variable=selectedAction,value=DrawAction.THIN_LINE.value)
boldLineRadioButton.grid(row=2, column=0)
boldLineLabel = Label(commandFrame, text=DrawAction.THIN_LINE.value)
boldLineLabel.grid(row=2, column=1)
boldLineRadioButton = Radiobutton(commandFrame,variable=selectedAction,value=DrawAction.BOLD_ERASER.value)
boldLineRadioButton.grid(row=3, column=0)
boldLineLabel = Label(commandFrame, text=DrawAction.BOLD_ERASER.value)
boldLineLabel.grid(row=3, column=1)
boldLineRadioButton = Radiobutton(commandFrame,variable=selectedAction,value=DrawAction.MEDIUM_ERASER.value)
boldLineRadioButton.grid(row=4, column=0)
boldLineLabel = Label(commandFrame, text=DrawAction.MEDIUM_ERASER.value)
boldLineLabel.grid(row=4, column=1)
boldLineRadioButton = Radiobutton(commandFrame,variable=selectedAction,value=DrawAction.THIN_ERASER.value)
boldLineRadioButton.grid(row=5, column=0)
boldLineLabel = Label(commandFrame, text=DrawAction.THIN_ERASER.value)
boldLineLabel.grid(row=5, column=1)
recognizeButton = Button(commandFrame, text=RECOGNIZE, command=partial(recognize))
recognizeButton.grid(row=6, column=1)
cleanButton = Button(commandFrame, text="clean", command=partial(clean))
cleanButton.grid(row=7, column=1)
# run program
resetBitmaps()
mainWindow.mainloop()