#!/usr/bin/env python3
# coding=utf-8
# import
from tkinter import Tk, Canvas, Label, Frame, LEFT, RIGHT
from neuralnetworkcli.neuralnetworkcli import endPointURL
from neuralnetworkcommon.service.service import ResourcePathType, ResourceValueType
from requests import put
from gzip import compress, decompress
from json import loads, dumps
from os import environ
# constants
PICTURE_LENGTH = 28
SCALING_FACTOR = 10
PERCEPTRON_ID = environ.get("DIGITS_OCR_PERCEPTRON_ID") #7101
DIGITS_RANGE = range(0,10)
CANVAS_SIZE = PICTURE_LENGTH*SCALING_FACTOR
URL=endPointURL+'/'+ResourcePathType.PERCEPTRON_EXECUTION.value.replace(ResourceValueType.PERCEPTRON.value,str(PERCEPTRON_ID))
RECOGNIZE="recognize"
DIGITS_SET_FILE="digitsSet.json.gz"
# shared variables
digitsLines = list()
# methods
def refreshResponse():
    for digit in DIGITS_RANGE:
        digitsLine = digitsLines[digit]
        digitNameLabel = digitsLine[0]
        digitNameLabel.config(background="gray85")
        digitValueLabel = digitsLine[1]
        digitValueLabel.config(text='-')
    pass
def callNeuralNetworkService(rawInputVector):
    print("image length : {}".format(len(rawInputVector)))
    compressedInputVector = compress(dumps(rawInputVector).encode())
    compressedOutputVector = put(URL, data=compressedInputVector)
    rawOutputVector = loads(decompress(compressedOutputVector.content).decode())
    print("response length : {}".format(len(rawOutputVector)))
    print("response : {}".format(rawOutputVector))
    return rawOutputVector
def displayResponse(outputVector):
    finalDigit = 0
    finalSimilarity = 0
    # for each digit
    for digit, similarity in enumerate(outputVector):
        digitsLine = digitsLines[digit]
        digitValueLabel = digitsLine[1]
        # display similarity
        digitValueLabel.config(text=str(similarity))
        # memorize final digit
        if similarity > finalSimilarity:
            finalDigit = digit
            finalSimilarity = similarity
        pass
    # highlight final digit
    finalDigitsLine = digitsLines[finalDigit]
    finalDigitNameLabel = finalDigitsLine[0]
    finalDigitNameLabel.config(background="red")
    pass
# main window construction
mainWindow = Tk()
mainWindow.title("manual draw OCR")
mainWindow.geometry(str(CANVAS_SIZE+450)+"x"+str(CANVAS_SIZE))
inputFrame = Frame(mainWindow)
inputFrame.pack(side = LEFT)
drawingFrame = Frame(inputFrame)
drawingFrame.pack(side = LEFT)
commandFrame = Frame(inputFrame)
commandFrame.pack(side = RIGHT)
resultFrame = Frame(mainWindow)
resultFrame.pack(side = RIGHT)
mainWindow.resizable(False, False)
canvas = Canvas(drawingFrame, width=CANVAS_SIZE, height=CANVAS_SIZE, background="white")
canvas.grid(row=0, column=0)
digitHeaderLabel = Label(resultFrame, text="digit")
digitHeaderLabel.grid(row=0, column=0)
similarityHeaderLabel = Label(resultFrame, text="similarity")
similarityHeaderLabel.grid(row=0, column=1)
for digit in DIGITS_RANGE:
    digitNameLabel = Label(resultFrame, text=str(digit))
    digitNameLabel.grid(row=digit+1, column=0)
    digitValueLabel = Label(resultFrame, text='-')
    digitValueLabel.grid(row=digit+1, column=1)
    digitsLines.append((digitNameLabel,digitValueLabel))
pass