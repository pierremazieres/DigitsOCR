#!/usr/bin/env python3
# coding=utf-8
# import
from neuralnetworktrainercli.neuralnetworktrainercli import endPointURL
from neuralnetworkcommon.service.service import TrainerResourcePathType, ResourceValueType
from requests import get
from neuralnetworkcommon.cli.cli import dumpArchive
from digitsocr.commonDigitsOCR import PERCEPTRON_ID, DIGITS_RANGE, DIGITS_SET_FILE
from gzip import decompress, compress
from neuralnetworkcommon.entity.trainingSession import TrainingSession
from json import dumps
from pythoncommontools.objectUtil.objectUtil import dumpObjetToDict
# get training session
URL=endPointURL+'/'+TrainerResourcePathType.SPECIFIC_TRAININGSESSION.value.replace(ResourceValueType.PERCEPTRON.value,PERCEPTRON_ID)
response = get(URL)
compressedTrainingSession = response.content
jsonTrainingSession = decompress(compressedTrainingSession).decode()
rawTrainingSession = TrainingSession.loadFromSimpleJson(jsonTrainingSession)
# transform training session to digits set
DIGITS_LIST = [list() for _ in DIGITS_RANGE]
for trainingElement in rawTrainingSession.testSet:
    digit = trainingElement.expectedOutput.index(1)
    DIGITS_LIST[digit].append(trainingElement.input)
    pass
jsonDigitsList = dumpObjetToDict(DIGITS_LIST)
encodedDigitsList = dumps(jsonDigitsList).encode()
compressedDigitsList = compress(encodedDigitsList)
dumpArchive(DIGITS_SET_FILE, compressedDigitsList)
pass