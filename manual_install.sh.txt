# update & install requirements
sudo apt-get update -y
sudo apt-get install -y wget python3 python3-psutil python3-numpy python3-psycopg2 python3-requests python3-tk
sudo ln -s /usr/local/lib/python3.* /usr/local/lib/python3
# download &  install neural network tools
rm -Rf /tmp/work
mkdir /tmp/work
cd /tmp/work
# WARNING : beware versions number are up-to-date
PYTHON_COMMON_TOOLS_VERSION=0.0.1
NEURAL_NETWORK_COMMON_VERSION=0.0.1
NEURAL_NETWORK_CLI_VERSION=0.0.0
DIGITS_OCR_VERSION=0.0.0
wget http://91.121.9.53:8008/tarball/PythonCommonTools/master/PythonCommonTools-${PYTHON_COMMON_TOOLS_VERSION}.linux-x86_64.tar.gz
wget http://91.121.9.53:8008/tarball/NeuralNetworkCommon/master/NeuralNetworkCommon-${NEURAL_NETWORK_COMMON_VERSION}.linux-x86_64.tar.gz
wget http://91.121.9.53:8008/tarball/NeuralNetworkCLI/master/NeuralNetworkCLI-${NEURAL_NETWORK_CLI_VERSION}.linux-x86_64.tar.gz
wget http://91.121.9.53:8008/tarball/DigitsOCR/master/DigitsOCR-${DIGITS_OCR_VERSION}.linux-x86_64.tar.gz
find . -name "*tar.gz" -exec tar -xzf {} \;
sudo cp -R usr/local/lib/python3.*/dist-packages/* /usr/local/lib/python3/dist-packages/
# set configuration
# WARNING : configuration is up-to-date
export NEURALNETWORK_TRAINER_CLI_HOST=91.121.9.53
export NEURALNETWORK_TRAINER_SERVICE_PORT=5000
export NEURALNETWORK_TRAINER_SERVICE_ENDPOINT=neuralnetwork/trainer
export DIGITS_OCR_PERCEPTRON_ID=7101
export NEURALNETWORK_CLI_HOST=91.121.9.53
export NEURALNETWORK_SERVICE_PORT=5010
export NEURALNETWORK_SERVICE_ENDPOINT=neuralnetwork
# get digits set
cd /usr/local/lib/python3/dist-packages/digitsocr
sudo -E python3 generateDigitsSet.py
# test
python3 manualDrawOCR.py
python3 presetDigitsOCR.py
rm -Rf /tmp/work
