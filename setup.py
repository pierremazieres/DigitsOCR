#!/usr/bin/env python3
# coding=utf-8
# import
from setuptools import setup, find_packages
# for version norm, see : https://www.python.org/dev/peps/pep-0440/#post-releases
# define setup parameters
setup(
    name="DigitsOCR",
    version="0.0.0",
    description="Digits OCR",
    packages=find_packages(),
    install_requires=["neuralnetworkcommon","neuralnetworkcli"],
    classifiers=[
        'Programming Language :: Python :: 3',
    ],
)
